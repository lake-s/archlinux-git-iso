# Archway Linux
<p>Archway linux is a Arch distro with Extended FS support and is going to have an installer in the future. Currently there it is just archlinux with extra FS support and linux-mainline.</p>
<p>linux-mainline is going to be replaced with normal linux upon the release of linux 6.7 in late December, because in linux 6.7 BcacheFS will be officialy supported</p>

## Compile
<p>First install dependencies</p>

```
pacman -S --needed archiso git
```

<p>Second install the chaotic-keyring before compiling</p>

```
pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
pacman-key --lsign-key 3056513887B78AEB
pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 
```

<p>After that you need to clone the repo and build the ISO</p>

```
git clone https://gitlab.com/archway-linux/archway.git
cd archway
mkarchiso -v -w ./WORK -o ./OUT ./archlive
```

<p>Now your ISO file will be located inside /path/to/archway-repo/OUT/</p>

## Install
<p>Follow the [Official Guide](https://wiki.archlinux.org/title/installation_guide#Connect_to_the_internet) at the format/mount part make sure to use your prefered filesystem and before the pacstrap Make sure to install the chaotic keyring</p>

```
pacman-key --recv-key 3056513887B78AEB --keyserver keyserver.ubuntu.com
pacman-key --lsign-key 3056513887B78AEB
pacman -U 'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst' 
```

<p>Then make sure to install linux-mainline as the kernel instead of linux and install your FS tools: BcacheFS = bcachefs-tools, ZFS = zfs-dkms, HFS/HFS+ = hfsprogs</p>

<p><strong>WARNING! There might be issues with bcachefs</strong></p>

<p>Make sure to install a AUR manager (ex: yay/paru) for updating ZFS,HFS,And linux-mainline</p>
